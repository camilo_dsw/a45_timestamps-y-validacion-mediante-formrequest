@extends('layouts.app')

@section('content')
<div class="container">
    <link href="/css/community.css" rel="stylesheet">
    @include('flash-message')
     <div class="row">
        <div class="col-md-8">
           
            <h1>
                <a  href="/community">community</a>
            </h1>
           @include('community.lista')

        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h3>Contribute a link</h3>
                </div>
                <div class="card-body">
                    <form method="POST" action="/community">
                        {{ csrf_field() }}

                        @include('community.add-link')

                        <div class="form-group">
                            <label for="title">Title:</label>

                            <input type="text" class="form-control" id="title" name="title" value="{{old('title')}}" placeholder="What is the title of your article?">

                        </div>

                        <div class="form-group">
                            <label for="link">Link:</label>
                            <input type="text" class="form-control" id="link" name="link" value="{{old('link')}}" placeholder="What is the URL?">

                        </div>

                        <div class="form-group card-footer">
                            <button class="btn btn-primary">Contribute Link</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
    {{$links->links()}}

</div>


@stop