<ul>
@if(count($links))
@foreach ($links as $link)
<li>
    <a  href="{{$link->link}}" target="_blank">
        <span class="label label-default" style="background: {{ $link->channel->color }}">
            {{ $link->channel->title }}
            </span>
        
    </a>
    <small>Contributed by: {{$link->creator->name}} {{$link->updated_at->diffForHumans()}}</small>
</li>
@endforeach
            
@else
<li class="Links__link">
    No contributions yet.
</li>    
@endif
</ul>